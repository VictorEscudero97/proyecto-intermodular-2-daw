<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;

require 'db.php';
require 'funciones.php';

return function (App $app) {
  
    //--------------POSTS--------------//

$app->get('/posts/top_rate', function(Request $request, Response $response){
  $sql = "SELECT * FROM Posts";
  try{
    $db = new db();
    $db = $db->conectDB();
    $resultado = $db->query($sql);
    if ($resultado->rowCount() > 0){
      $users = $resultado->fetchAll(PDO::FETCH_OBJ);
      $response->getBody()->write(json_encode($users));
      return $response;
    }else {
      $response->getBody()->write(json_encode("No existen Users en la BBDD."));
      return $response;
    }
    $resultado = null;
    $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
}); 

$app->get('/posts/most_view', function(Request $request, Response $response){
  $sql = "SELECT * FROM Posts";
  try{
    $db = new db();
    $db = $db->conectDB();
    $resultado = $db->query($sql);
    if ($resultado->rowCount() > 0){
      $users = $resultado->fetchAll(PDO::FETCH_OBJ);
      $response->getBody()->write(json_encode($users));
      return $response;
    }else {
      $response->getBody()->write(json_encode("No existen Users en la BBDD."));
      return $response;
    }
    $resultado = null;
    $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
}); 
//--------------endPOSTS--------------//

//--------------USERS--------------//

//**GET**

$app->get('/users/all', function(Request $request, Response $response){
  $sql = "SELECT * FROM Users";
  try{
    $db = new db();
    $db = $db->conectDB();
    $resultado = $db->query($sql);

    if ($resultado->rowCount() > 0){
      $user = $resultado->fetchAll(PDO::FETCH_OBJ);
      $response->getBody()->write(json_encode($user));
      return $response;
    }else {
      $response->getBody()->write(json_encode("No existen Users en la BBDD."));
      return $response;
    }
    $resultado = null;
    $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
});
//GET Recueperar Posts por Users ID (ok)
$app->get('/users/{id}/posts', function(Request $request, Response $response){
  $id_user = $request->getAttribute('id');
  $sql = "SELECT * FROM Posts WHERE id_user = $id_user";
  try{
    $db = new db();
    $db = $db->conectDB();
    $resultado = $db->query($sql);

    if ($resultado->rowCount() > 0){
      $user = $resultado->fetchAll(PDO::FETCH_OBJ);
      $response->getBody()->write(json_encode($user));
      return $response;
    }else {
      $response->getBody()->write(json_encode("No existen posts"));
      return $response;
    }
    $resultado = null;
    $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
});
//GET Recueperar Users por ID (ok)
$app->get('/users/{id}', function(Request $request, Response $response){
    $id_user = $request->getAttribute('id');
    $sql = "SELECT * FROM Users WHERE id_user = $id_user";
    try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);
  
      if ($resultado->rowCount() > 0){
        $user = $resultado->fetchAll(PDO::FETCH_OBJ);
        $response->getBody()->write(json_encode($user));
        return $response;
      }else {
        $response->getBody()->write(json_encode("No existen Users en la BBDD con ese id."));
        return $response;
      }
      $resultado = null;
      $db = null;
    }catch(PDOException $e){
        $response->getBody()->write(json_encode("Error: ".$e));
        return $response;
    }
});

//GET Devolver un usuario registrado con coincidencia de username y password
$app->get('/users/{username}/c/{password}', function(Request $request, Response $response){
  $user = $request->getAttribute('username');
  $pass = $request->getAttribute('password');

  $sql = "SELECT * FROM USERS WHERE usename ='$user' AND password = SHA1('$pass')";
  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);

      if ($resultado->rowCount() > 0){
          $user = $resultado->fetchAll(PDO::FETCH_OBJ);
          $response->getBody()->write(json_encode($user));
          return $response;
      }else {
          $response->getBody()->write(json_encode("El usuario no ha sido encontrado"));
          return $response;
      }
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
});

//**POST**

//POST crear nuevo Post (ok)
$app->post('/posts/new_post', function(Request $request, Response $response){

  $json = $request->getBody();
  $arr = json_decode($json, true);

  if (comprobarPost($arr['title']) == false){
    $sql = "INSERT INTO `posts` (`id_post`, `photo`, `text`, `title`, `likes`, `approved`, `id_user`, `date`, `tag`, `time`) 
    VALUES (NULL, :photo, :textt, :title, 0, 0, :id_user, NOW(), :tags, :timer)";
    try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->prepare($sql);
      
      // $likes_int = intval($arr['likes']);
      $resultado->bindParam(':textt', $arr['text']);
      $resultado->bindParam(':title', $arr['title']);
      //$resultado->bindParam(':likes', $arr['likes']);
      $resultado->bindParam(':photo', $arr['photo']);
      //$resultado->bindParam(':approved', $arr['approved']);
      $resultado->bindParam(':id_user', $arr['id_user']);
      $resultado->bindParam(':tags', $arr['tags']);
      $resultado->bindParam(':timer', $arr['time']);
  
      $resultado->execute();
      $response->getBody()->write(json_encode("Nuevo post guardado."));

      return $response;
      $resultado = null;
      $db = null;

    }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
    }
  }else{
    $response->getBody()->write(json_encode('Post Repetido'));
    return $response;
  }
});
//POST crear nuevo User (ok)
$app->post('/users/new_user', function(Request $request, Response $response){
 
  $json = $request->getBody();
  $arr = json_decode($json, true);
 
  if (comprobarUser($arr['email']) == false && comprobarUsername($arr['username']) == false){
    $sql = "INSERT INTO `users` (`id_user`, `name`, `email`, `password`, `bio`, `photo`, `usename`, `admin`) 
    VALUES (NULL, :nombre, :email, SHA1(:passwor), :bio, :photo, :user_name, :admi)";
    try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->prepare($sql);
      
      $resultado->bindParam(":nombre", $arr['name']);
      $resultado->bindParam(":email", $arr['email']);
      $resultado->bindParam(":passwor", $arr['password']);
      $resultado->bindParam(":user_name", $arr['username']);
      $resultado->bindParam(":bio", $arr['bio']);
      $resultado->bindParam(":photo", $arr['photo']);
      $resultado->bindParam(":admi", $arr['admin']);
  
       $resultado->execute();
       $response->getBody()->write(json_encode("Nuevo usuario guardado."));
        return $response;
       $resultado = null;
       $db = null;
    }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
    }
  }else{
    $response->getBody()->write(json_encode('Usuario Repetido:'.comprobarUser($arr['email'])));
    return $response;
  }
  
});


//--------------endUSERS--------------//

//--------------ADMIN-----------------//
//**GET**
//GET Post sin aprobar (ok)
$app->get('/admin/approved/no', function(Request $request, Response $response){
    $sql = "SELECT * FROM Posts WHERE approved = 0";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->query($sql);

        if ($resultado->rowCount() > 0){
            $user = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($user));
            return $response;
        }else {
            $response->getBody()->write(json_encode("No existen Users en la BBDD con ese id."));
            return $response;
        }
        $resultado = null;
        $db = null;
    }catch(PDOException $e){
        $response->getBody()->write(json_encode("Error: ".$e));
        return $response;
    }

});

//GET Post aprobados 
$app->get('/admin/approved/yes', function(Request $request, Response $response){
    $sql = "SELECT * FROM Posts WHERE approved = 1";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->query($sql);

        if ($resultado->rowCount() > 0){
            $user = $resultado->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($user));
            return $response;
        }else {
            $response->getBody()->write(json_encode("No existen Users en la BBDD con ese id."));
            return $response;
        }
        $resultado = null;
        $db = null;
    }catch(PDOException $e){
        $response->getBody()->write(json_encode("Error: ".$e));
        return $response;
    }

});

//**POST**
//POST edit user (ok)
$app->post('/admin/edit_user/{username}', function(Request $request, Response $response){


    $username = $request->getAttribute('username');

    $json = $request->getBody();
    $arr = json_decode($json, true);

    $sql = "UPDATE `Users` 
                SET `name`= :nombre, `email`= :email, `password`= SHA1(:passwor) , `bio`= :bio, `photo`= :photo, `usename`= :username, `admin`= :admi   
                WHERE `usename`= :usernamee";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);

        $resultado->bindParam(':nombre', $arr['name']);
        $resultado->bindParam(':email', $arr['email']);
        $resultado->bindParam(':passwor', $arr['password']);
        $resultado->bindParam(':bio', $arr['bio']);
        $resultado->bindParam(':photo', $arr['photo']);
        $resultado->bindParam(':username', $arr['username']);
        $resultado->bindParam(':admi', $arr['admin']);
        $resultado->bindParam('usernamee', $username);

        $resultado->execute();
        $response->getBody()->write(json_encode("Usuario editado correctamente."));

        return $response;
        $resultado = null;
        $db = null;
    }catch(PDOException $e){
        $response->getBody()->write(json_encode("Error: ".$e));
        return $response;
    }

});


//POST edit post (ok)
$app->post('/admin/edit_post/{id}', function(Request $request, Response $response){


    $id_post = $request->getAttribute('id');

    $json = $request->getBody();
    $arr = json_decode($json, true);

    $sql = "UPDATE `Posts` 
                SET `photo`= :photo, `text`= :tex, `title`= :title, `likes`= :likes, `approved`= :approved, `id_user`= :id_user, `tag`= :tags, `time`= :tim   
                WHERE `id_post`= :id_post";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);

        $resultado->bindParam(':photo', $arr['photo']);
        $resultado->bindParam(':tex', $arr['text']);
        $resultado->bindParam(':title', $arr['title']);
        $resultado->bindParam(':likes', $arr['likes']);
        $resultado->bindParam(':approved', $arr['approved']);
        $resultado->bindParam(':id_user', $arr['id_user']);
        $resultado->bindParam(':tags', $arr['tags']);
        $resultado->bindParam(':tim', $arr['time']);
        $resultado->bindParam(':id_post', $id_post);

        $resultado->execute();
        $response->getBody()->write(json_encode("Post editado correctamente."));

        return $response;
        $resultado = null;
        $db = null;
    }catch(PDOException $e){
        $response->getBody()->write(json_encode("Error: ".$e));
        return $response;
    }

});

//POST delete post
$app->delete('/admin/delete_post/{id}', function(Request $request, Response $response){


  $id_post = $request->getAttribute('id');

  $sql = "DELETE FROM `Posts` 
          WHERE `id_post`= :id_post";

  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->prepare($sql);

      $resultado->bindParam(':id_post', $id_post);

      $resultado->execute();
      $response->getBody()->write(json_encode("Post eliminado correctamente."));

      return $response;
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }

});

//POST delete user
$app->delete('/admin/delete_user/{id}', function(Request $request, Response $response){


  $id_user = $request->getAttribute('id');

  $sql = "DELETE FROM `Users` 
          WHERE `id_user`= :id_user";

  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->prepare($sql);

      $resultado->bindParam(':id_user', $id_user);

      $resultado->execute();
      $response->getBody()->write(json_encode("Usuario eliminado correctamente."));

      return $response;
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }

});

//POST SET APPROVED TO 1 (TRUE)
$app->post('/admin/edit_post/{id}/approve', function(Request $request, Response $response){

  $id_post = $request->getAttribute('id');

  $sql = "UPDATE `Posts` 
              SET  `approved`=  1
              WHERE `id_post`= :id_post";

  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->prepare($sql);

      $resultado->bindParam(':id_post', $id_post);

      $resultado->execute();
      $response->getBody()->write(json_encode("Post aprobado."));

      return $response;
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }

});


//--------------endADMIN-----------------//

//Buscar posts por tag
$app->get('/post/{tag}', function(Request $request, Response $response){
  $tag = $request->getAttribute('tag');

  $sql = "SELECT * FROM Posts WHERE tag LIKE '%$tag%'";
  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);

      if ($resultado->rowCount() > 0){
          $posts = $resultado->fetchAll(PDO::FETCH_OBJ);
          $response->getBody()->write(json_encode($posts));
          return $response;
      }else {
          $response->getBody()->write(json_encode("El tag no ha sido encontrado"));
          return $response;
      }
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
});
//Buscar posts por título relativo
$app->get('/title/{title}', function(Request $request, Response $response){
  $title = $request->getAttribute('title');

  $sql = "SELECT * FROM Posts WHERE title LIKE '$title%'";
  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);

      if ($resultado->rowCount() > 0){
          $posts = $resultado->fetchAll(PDO::FETCH_OBJ);
          $response->getBody()->write(json_encode($posts));
          return $response;
      }else {
          $response->getBody()->write(json_encode("El tag no ha sido encontrado"));
          return $response;
      }
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
});
//Buscar posts por título exacto
$app->get('/title/exact/{title}', function(Request $request, Response $response){
  $title = $request->getAttribute('title');

  $sql = "SELECT * FROM Posts WHERE title = '$title'";
  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);

      if ($resultado->rowCount() > 0){
          $posts = $resultado->fetchAll(PDO::FETCH_OBJ);
          $response->getBody()->write(json_encode($posts));
          return $response;
      }else {
          $response->getBody()->write(json_encode("El post no ha sido encontrado"));
          return $response;
      }
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
});


//POST rate post (ok)
$app->post('/admin/rate_post/{id}', function(Request $request, Response $response){


  $id_post = $request->getAttribute('id');

  $json = $request->getBody();
  $arr = json_decode($json, true);

  $sql = "UPDATE `Posts` 
              SET `likes`= :likes, `total_value`= :total_value, `total_votes`= :total_votes   
              WHERE `id_post`= :id_post";

  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->prepare($sql);

      $resultado->bindParam(':total_value', $arr['total_value']);
      $resultado->bindParam(':total_votes', $arr['total_votes']);
      $resultado->bindParam(':likes', $arr['likes']);
      $resultado->bindParam(':id_post', $id_post);

      $resultado->execute();
      $response->getBody()->write(json_encode("Post rating actualizado correctamente."));

      return $response;
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }

});
//POST rate user (ok)
$app->post('/admin/rate_user/{id}', function(Request $request, Response $response){


  $id_user = $request->getAttribute('id');

  $json = $request->getBody();
  $arr = json_decode($json, true);

  $sql = "UPDATE `Users` 
              SET `rated_post`= :rated_post
              WHERE `id_user`= :id_user";

  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->prepare($sql);

      $resultado->bindParam(':rated_post', $arr['rated_post']);
      $resultado->bindParam(':id_user', $id_user);

      $resultado->execute();
      $response->getBody()->write(json_encode("Rating editado correctamente."));

      return $response;
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }

});

$app->get('/top', function(Request $request, Response $response){

  $sql = "SELECT * FROM Posts WHERE approved = 1 ORDER BY likes DESC";
  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);

      if ($resultado->rowCount() > 0){
          $posts = $resultado->fetchAll(PDO::FETCH_OBJ);
          $response->getBody()->write(json_encode($posts));
          return $response;
      }else {
          $response->getBody()->write(json_encode("El tag no ha sido encontrado"));
          return $response;
      }
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
});

$app->get('/date', function(Request $request, Response $response){

  $sql = "SELECT * FROM Posts WHERE approved = 1 ORDER BY date DESC";
  try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);

      if ($resultado->rowCount() > 0){
          $posts = $resultado->fetchAll(PDO::FETCH_OBJ);
          $response->getBody()->write(json_encode($posts));
          return $response;
      }else {
          $response->getBody()->write(json_encode("El tag no ha sido encontrado"));
          return $response;
      }
      $resultado = null;
      $db = null;
  }catch(PDOException $e){
      $response->getBody()->write(json_encode("Error: ".$e));
      return $response;
  }
});

};