setTimeout(()=>{
   // ---------Responsive-navbar-active-animation-----------
  function test(){
    var tabsNewAnim = $('#navbarSupportedContent');
    var selectorNewAnim = $('#navbarSupportedContent').find('li').length;
    var activeItemNewAnim = tabsNewAnim.find('.active');
    var activeWidthNewAnimHeight = activeItemNewAnim.innerHeight();
    var activeWidthNewAnimWidth = activeItemNewAnim.innerWidth();
    var itemPosNewAnimTop = activeItemNewAnim.position();
    var itemPosNewAnimLeft = activeItemNewAnim.position();
    $(".hori-selector").css({
      "top":itemPosNewAnimTop.top + "px",
      "left":itemPosNewAnimLeft.left + "px",
      "height": activeWidthNewAnimHeight + "px",
      "width": activeWidthNewAnimWidth + "px"
    });
    $("#navbarSupportedContent").on("click","li",function(e){
      $('#navbarSupportedContent ul li').removeClass("active");
      $(this).addClass('active');
      var activeWidthNewAnimHeight = $(this).innerHeight();
      var activeWidthNewAnimWidth = $(this).innerWidth();
      var itemPosNewAnimTop = $(this).position();
      var itemPosNewAnimLeft = $(this).position();
      $(".hori-selector").css({
        "top":itemPosNewAnimTop.top + "px",
        "left":itemPosNewAnimLeft.left + "px",
        "height": activeWidthNewAnimHeight + "px",
        "width": activeWidthNewAnimWidth + "px"
      });
    });
  }

  $(document).ready(function(){
    setTimeout(function(){ test(); });
  });
  $(window).on('resize', function(){
    setTimeout(function(){ test(); }, 500);
  });
  $(".navbar-toggler").click(function(){
    setTimeout(function(){ test(); });
  });


  // Attaching the event listener function to window's resize event
  window.addEventListener("resize", displayWindowSize);
  document.getElementById('fff').addEventListener("click", ps());
  // Calling the function for the first time
  displayWindowSize();


  
}, 100);

  // Defining event listener function
function displayWindowSize(){
    // Get width and height of the window excluding scrollbars
    var w = document.documentElement.clientWidth;
    var h = document.documentElement.clientHeight;
  
    // Display result inside a div element
  
    if (w <= 993){
      document.getElementById("profile").style.display = "none";
    }else{
      document.getElementById("profile").style.display = "inline";
    }
}

function ps(){
  let perfil = document.getElementById("profile");
  if (perfil.style.display == "none"){
    document.getElementById("profile").style.display = "inline";
  }else{
    document.getElementById("profile").style.display = "none";
  }

  console.log("hola");
}

function perfil_show(){
  console.log('e')
  let perfil = document.getElementById("profile");
  if (perfil.style.display == "none"){
    document.getElementById("profile").style.display = "inline";
  }else{
    displayWindowSize();
  }

  }
  
  window.onscroll = function (e) 
  {
    /* 
      Obtenemos todos los elementos que tengan la clase animate__animated y sean hijos de la clase scroll-animations
      Busca información sobre la función querySelectorAll
    */

    var elements = [] // TO DO

    elements = document.querySelectorAll(".scroll-animations .animate__animated");

    
    // Recorremos la lista de elementos
    for (var i = 0; i < elements.length; i++) 
    {
      // Comprobamos si están en el viewport
      if (isInViewport(elements[i]) === true) 
      {
        // En caso afirmativo agregamos la clase de animación fadeInLeft
        elements[i].classList.add("animate__backInRight");
      }
    }
  }

  // Función para detectar si un elemento está siendo visualizado (se encuentra dentro del viewport)
  function isInViewport(element) 
  {
    var rect = element.getBoundingClientRect();
    var html = document.documentElement;
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || html.clientHeight) &&
      rect.right <= (window.innerWidth || html.clientWidth)
    );
  }