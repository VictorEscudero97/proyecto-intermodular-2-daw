import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class ModelServiceService {
  private data = new BehaviorSubject<any>('');
  cast = this.data.asObservable();

  private type = new BehaviorSubject<any>('');
  type_cast = this.type.asObservable();

  setItem(item: JSON){
    this.data.next(item);
  }

  getItem(){
    return this.cast;

  }

  setType(type: string){
    this.type.next(type);
  }

  constructor() {
   }
}
