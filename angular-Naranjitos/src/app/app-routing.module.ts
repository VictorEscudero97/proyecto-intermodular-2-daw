import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsInicioDisplayComponent } from './cards-inicio-display/cards-inicio-display.component';
import { GalleryComponent } from './gallery';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegistroPageComponent } from './registro-page/registro-page.component';
import { MenuAdminComponent } from './menu-admin/menu-admin.component';
import { MenuAdminPosteditComponent } from './menu-admin-postedit/menu-admin-postedit.component';
import { ProfileComponent } from './profile/profile.component';
import { BigPostComponent } from './big-post/big-post.component';
import { BigGaleriaComponent } from './big-galeria/big-galeria.component';
import { BigGaleriaTituloComponent } from './big-galeria-titulo/big-galeria-titulo.component';
import { SetTingsComponent } from './set-tings/set-tings.component';

const routes: Routes = [
  {path: 'cards', component: CardsInicioDisplayComponent },
  {path: 'home', component: HomeComponent },
  {path: 'gallery', component: GalleryComponent },
  {path: 'login', component: LoginComponent },
  {path: 'register', component: RegistroPageComponent },
  {path: 'admin', component: MenuAdminComponent },
  {path: 'post', component: MenuAdminPosteditComponent },
  {path: 'profile/:id', component: ProfileComponent },
  {path: 'bigpost/:title', component: BigPostComponent },
  {path: 'biggalery/:tag', component: BigGaleriaComponent },
  {path: 'biggalery2/:tag', component: BigGaleriaTituloComponent },
  {path: 'settings', component: SetTingsComponent },
  {path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
