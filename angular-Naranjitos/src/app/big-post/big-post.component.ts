import { Component, OnInit } from '@angular/core';
import { PostServiceService } from '../services/post-service.service';
import {
  faCalendarAlt,
  faClock,
  faStar,
} from '@fortawesome/free-solid-svg-icons';
import { ApiHttpService } from '../services/api-http.service';
import { ApiEndpointsService } from '../services/api-endpoints.service';
import { Constants } from '../config/constants';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-big-post',
  templateUrl: './big-post.component.html',
  styleUrls: ['./big-post.component.css'],
  providers: [ApiHttpService, ApiEndpointsService, Constants],
})
export class BigPostComponent implements OnInit {
  //valor que se recibe cuando el usuario vota el post actual
  rating!: number;

  post_data!: any;
  user_data!: any;

  //values from post
  total_votes!: number;
  total_value!: number;
  actual_post_rating!: number;
  //values from user
  posts_rated!: string;

  faStar = faStar;
  faClock = faClock;
  faCalendarAlt = faCalendarAlt;
  item: any;
  selectedItemData: any;
  _subscription: any;
  constructor(
    private PostServiceService: PostServiceService,
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService,
    private _http: HttpClient,
    private _router2: Router,
    private _router: ActivatedRoute
  ) {
    this.PostServiceService.cast.subscribe((result) => {
      this.selectedItemData = result;
      console.log(this.selectedItemData);
      window.scroll(0, 0);

      if (this.selectedItemData != []) {
        var object: any = {};

        var json = JSON.stringify(this.selectedItemData);
        //console.log(json);

        localStorage.setItem('post', json);
        //console.log(localStorage.getItem('post'));
      } else {
        let value_last_post = localStorage.getItem('post');
        if (value_last_post !== null) {
          this.selectedItemData = JSON?.parse(value_last_post);
          //console.log(this.selectedItemData);
        }
      }
    });
  }

  ngOnInit(): void {
    this.getPost();
    this.getUser();
  }

  star1() {
    this.rating = 1;
    this.setNewRatingValues();
    this.vote();
    if(this.isPostVoted() === true){
      alert('Has votado con ' + this.rating + 'estrellas');
    }
    else{
      alert('Has eliminado tu voto correctamente');
    }
  }
  star2() {
    this.rating = 2;
    this.setNewRatingValues();
    this.vote();
    if(this.isPostVoted() === true){
      alert('Has votado con ' + this.rating + 'estrellas');
    }
    else{
      alert('Has eliminado tu voto correctamente');
    }
  }
  star3() {
    this.rating = 3;
    this.setNewRatingValues();
    this.vote();
    if(this.isPostVoted() === true){
      alert('Has votado con ' + this.rating + 'estrellas');
    }
    else{
      alert('Has eliminado tu voto correctamente');
    }
  }
  star4() {
    this.rating = 4;
    this.setNewRatingValues();
    this.vote();
    if(this.isPostVoted() === true){
      alert('Has votado con ' + this.rating + 'estrellas');
    }
    else{
      alert('Has eliminado tu voto correctamente');
    }
  }
  star5() {
    this.rating = 5;
    this.setNewRatingValues();
    this.vote();
    if(this.isPostVoted() === true){
      alert('Has votado con ' + this.rating + 'estrellas');
    }
    else{
      alert('Has eliminado tu voto correctamente');
    }
  }

  getPost() {
    let titulo = this._router.snapshot.params.title;
    this._http
      .get(this.apiEndpointsService.getPostsByExactTitle(titulo))
      .subscribe(
        (response) => {
          this.post_data = response;
          this.post_data = this.post_data[0];
          this.total_value = parseInt(this.post_data.total_value);
          this.total_votes = parseInt(this.post_data.total_votes);
          console.log('SUUU',response);
          this.actual_post_rating = parseInt(this.post_data.likes);
        },
        (error) => console.log(error)
      );
  }

  //esto va a funcionar aunque el usuario no esté registrado, hay que adaptarlo
  getUser() {
    let user_id = localStorage.getItem('user')!;
    this._http.get(this.apiEndpointsService.getUserById(user_id)).subscribe(
      (response) => {
        this.user_data = response;
        this.user_data = this.user_data[0];
        //console.log('user: ', this.user_data.rated_post);
        this.posts_rated = this.user_data.rated_post;
      },
      (error) => console.log(error)
    );
  }

  setNewRatingValues() {
    if (this.isPostVoted()) {
      this.total_votes--;
      if (this.total_value <= 0) {
        this.total_value = 1;
      }
      //
      let rated_posts = this.posts_rated.split(',');
      let aux_value!: number;
      for (let i = 0; i < rated_posts.length; i++) {
        var arr_ud = rated_posts[i].split('-');
        if (this.post_data.id_post == arr_ud[0]) {
          aux_value = parseInt(arr_ud[1]);
        }
      }
      console.log('aux value: ', aux_value);
      this.total_value = this.total_value - aux_value;
      console.log('TOTAL VALUE: ', this.total_value);
      //
      this.actual_post_rating =
        Math.round((this.total_value / this.total_votes) * 10) / 10;
      console.log('ACTUAL RATING RESTA: ', this.actual_post_rating);
      //AQUI TENGO QUE UPDATEAR LA BASE DE DATOS
      let data = JSON.stringify({
        "likes": this.actual_post_rating,
        "total_value": this.total_value,
        "total_votes": this.total_votes
      });
      this._http
        .post(
          this.apiEndpointsService.post_rating_update(this.post_data.id_post),
          data
        )
        .subscribe(
          (response) => {
            console.log(response);
          },
          (error) => console.log(error)
        );
    } else {
      this.total_votes++;
      console.log('total_votes: ', this.total_votes);
      this.total_value = this.total_value + this.rating;
      this.actual_post_rating =
        Math.round((this.total_value / this.total_votes) * 10) / 10;
      //AQUI TENGO QUE UPDATEAR LA BASE DE DATOS
      let data = JSON.stringify({
        "likes": this.actual_post_rating,
        "total_value": this.total_value,
        "total_votes": this.total_votes
      });
      console.log('data: ', data);
      this._http
        .post(
          this.apiEndpointsService.post_rating_update(this.post_data.id_post),
          data
        )
        .subscribe(
          (response) => {
            console.log(response);
          },
          (error) => console.log(error)
        );
    }

    console.log(
      'total_votes = ',
      this.total_votes,
      '\n',
      'total_value = ',
      this.total_value,
      '\n',
      'actual_rating = ',
      this.actual_post_rating
    );
  }

  isPostVoted() {
    let isvoted: boolean = false;
    let rated_posts = this.posts_rated.split(',');
    for (let i = 0; i < rated_posts.length; i++) {
      let arr_ud = rated_posts[i].split('-');
      if (this.post_data.id_post == arr_ud[0]) {
        isvoted = true;
      }
    }
    return isvoted;
  }

  vote() {
    if (this.isPostVoted()) {
      //borrar de la actual lista de posts votados el que se intenta votar
      let rated_posts = this.posts_rated.split(',');
      let string_aux = '';
      for (let i = 0; i < rated_posts.length; i++) {
        let arr_ud = rated_posts[i].split('-');
        console.log(arr_ud);
        if (this.post_data.id_post == arr_ud[0]) {
          rated_posts.splice(i, 1);
        }
        if (arr_ud.length == 0) {
          rated_posts.splice(i, 1);
        }
      }
      //ahora subir el nuevo conjunto de valores sin el eliminado
      if (rated_posts.length > 0) {
        if (rated_posts[0] != undefined && rated_posts[0] != null) {
          for (let j = 0; j < rated_posts.length; j++) {
            string_aux = string_aux + rated_posts[j] + ',';
          }
          while (string_aux.indexOf(',') == 0) {
            string_aux = string_aux.substring(1, string_aux.length);
          }
          while (
            string_aux.substring(
              string_aux.length - 2,
              string_aux.length - 1
            ) == ','
          ) {
            string_aux = string_aux.substring(0, string_aux.length - 1);
          }
          //enviar lo mismo que en el otro caso
          //console.log('despues de borrar: ', string_aux);
          this.posts_rated = string_aux;
          let data = JSON.stringify({ rated_post: this.posts_rated });
          this._http
            .post(this.apiEndpointsService.vote(this.user_data.id_user), data)
            .subscribe(
              (response) => {
                console.log(response);
              },
              (error) => console.log(error)
            );
        }
      }
      //si pasa algo más va aqui
    } else {
      //console.log('esto: ', this.posts_rated);
      this.posts_rated =
        this.posts_rated + this.post_data.id_post + '-' + this.rating + ',';
      let data = JSON.stringify({ rated_post: this.posts_rated });
      //console.log('data', data);
      this._http
        .post(this.apiEndpointsService.vote(this.user_data.id_user), data)
        .subscribe(
          (response) => {
            console.log(response);
          },
          (error) => console.log(error)
        );
    }
  }

  getDataFromSelectedItem() {
    console.log(this.selectedItemData.id_user);
    // this.ProfileServiceService.setItem(this.selectedItemData.id_user);
    this._router2.navigate(['/profile', this.selectedItemData.id_user]);
  }
}
