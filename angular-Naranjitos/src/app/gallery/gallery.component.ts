import { Component, OnInit } from '@angular/core';
import {
  faSearch
} from '@fortawesome/free-solid-svg-icons';

import { GaleryServiceService } from '../services/galery-service.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  faSearch = faSearch;

  constructor(
    private GaleryServiceService: GaleryServiceService,
    private _router: ActivatedRoute,
    private _router2: Router
  ) { }

  ngOnInit(): void {
  }

  buscar(value:string) {
    console.log(value);
    this.GaleryServiceService.setItem(value);
    this._router2.navigate(['/biggalery',value]);
  }
}
