import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BigGaleriaComponent } from './big-galeria.component';

describe('BigGaleriaComponent', () => {
  let component: BigGaleriaComponent;
  let fixture: ComponentFixture<BigGaleriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BigGaleriaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BigGaleriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
