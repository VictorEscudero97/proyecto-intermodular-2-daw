import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';

//pruebas
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
//fin_pruebas
//prueba nueva forma request
import { ApiHttpService } from '../services/api-http.service';
import { ApiEndpointsService } from '../services/api-endpoints.service';
import { Constants } from '../config/constants';

import { GaleryServiceService } from '../services/galery-service.service';
@Component({
  selector: 'app-big-galeria',
  templateUrl: './big-galeria.component.html',
  styleUrls: ['./big-galeria.component.css'],
  providers: [ApiHttpService, ApiEndpointsService, Constants]
})
@Injectable()
export class BigGaleriaComponent implements OnInit {

  @Input() petition: string = '';
  array_post: Array<any> = [];
  tag: string= '';
  constructor(
    private GaleryServiceService: GaleryServiceService,
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService,
    private _router: ActivatedRoute
  ) {
    this.tag = this._router.snapshot.params.tag;
    console.log(this._router.snapshot.params.tag)
    this.GaleryServiceService.setItem(this.tag);
  }

  ngOnInit(): void {
    // this.apiHttpService
    //       .get(this.apiEndpointsService.getAllPosts())
    //       .subscribe((result: any) => {
    //         console.log(result);
    //         this.array_post = [];
    //         for (let i = 0; i < result.length; i++) {
    //           this.array_post.push(result[i]);
    //         }
    //         console.log(this.array_post);
    //         //this.array_users.push(result.data);
    //         //console.log(this.array_users);
    //       });

  }

}
