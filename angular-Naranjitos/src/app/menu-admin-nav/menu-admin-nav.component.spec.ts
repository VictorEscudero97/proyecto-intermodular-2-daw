import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuAdminNavComponent } from './menu-admin-nav.component';

describe('MenuAdminNavComponent', () => {
  let component: MenuAdminNavComponent;
  let fixture: ComponentFixture<MenuAdminNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuAdminNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuAdminNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
