import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiHttpService } from '../services/api-http.service';
import { ApiEndpointsService } from '../services/api-endpoints.service';
import { Constants } from '../config/constants';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ModelServiceService } from '../model-service.service';

@Component({
  selector: 'app-menu-admin-nav',
  templateUrl: './menu-admin-nav.component.html',
  styleUrls: ['./menu-admin-nav.component.css'],
  providers: [ApiHttpService, ApiEndpointsService, Constants],
})
export class MenuAdminNavComponent implements OnInit {
  @Input() type: string = 'users';
  array_lista: Array<any> = [];
  array_users: Array<any> = [];
  array_posts_approved: Array<any> = [];
  array_posts_unapproved: Array<any> = [];
  selectedItem: any;
  action!: string;
  @Output() evento_select = new EventEmitter();

  constructor(
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService,
    private modelServiceService: ModelServiceService
  ) {}

  ngOnInit(): void {
    this.getAllUsers();
    this.getAllPostsApproved();
    this.getAllPostsUnapproved();
  }

  getDataFromSelectedItem(data: any) {
    for (let i = 0; i < this.array_lista.length; i++) {
      if (this.type == 'users') {
        if (this.array_lista[i].usename == data) {
          this.selectedItem = this.array_lista[i];
          this.modelServiceService.setItem(<JSON>this.selectedItem);
        }
      }
      if (this.type == 'posts_approved' || this.type == 'posts_unapproved') {
        if (this.array_lista[i].title == data) {
          this.selectedItem = this.array_lista[i];
          this.modelServiceService.setItem(<JSON>this.selectedItem);
        }
      }
    }
  }

  setTypeUsers() {
    this.type = 'users';
    this.sendEvento(this.type);
    this.array_lista = this.array_users;
  }

  setTypePosts_approved() {
    this.type = 'posts_approved';
    this.sendEvento(this.type);
    this.array_lista = this.array_posts_approved;
  }

  setTypePosts_unapproved() {
    this.type = 'posts_unapproved';
    this.sendEvento(this.type);
    this.array_lista = this.array_posts_unapproved;
  }

  setTypeOfAction(type: string) {
    this.action = type;
    this.modelServiceService.setType(this.action);
  }

  sendEvento(evento: any) {
    this.evento_select.emit(evento);
  }

  getAllUsers() {
    this.apiHttpService
      .get(this.apiEndpointsService.getAllUsers())
      .subscribe((result: any) => {
        this.array_users = [];
        for (let i = 0; i < result.length; i++) {
          this.array_users.push(result[i]);
        }
        console.log('ARRAY USERS: ', this.array_users);
      });
  }

  getAllPostsApproved() {
    this.apiHttpService
      .get(this.apiEndpointsService.getPostsApproved())
      .subscribe((result: any) => {
        this.array_posts_approved = [];
        for (let i = 0; i < result.length; i++) {
          this.array_posts_approved.push(result[i]);
        }
      });
  }

  getAllPostsUnapproved() {
    this.apiHttpService
      .get(this.apiEndpointsService.getPostsUnapproved())
      .subscribe((result: any) => {
        this.array_posts_unapproved = [];
        for (let i = 0; i < result.length; i++) {
          this.array_posts_unapproved.push(result[i]);
        }
      });
  }
}
