import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardsInicioDisplayComponent } from './cards-inicio-display/cards-inicio-display.component';
//ajax/http request
import { HttpClientModule } from '@angular/common/http';
//end ajax/http request
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
//prueba nueva forma request
import { Constants } from './config/constants';
import { ApiHttpService } from './services/api-http.service';
import { ApiEndpointsService } from './services/api-endpoints.service';
//fin forma request
import { NavBarComponent } from './nav-bar/nav-bar.component';
// inicio menu
import { HomeComponent } from './home/home.component';
import { GalleryComponent } from './gallery/gallery.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './footer/footer.component';
//fin menu
//registro
import { RegistroComponent } from './registro/registro.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
//login
import { LoginFormComponent } from './login-form/login-form.component';
import { RegistroPageComponent } from './registro-page/registro-page.component';
import { MenuAdminComponent } from './menu-admin/menu-admin.component';
import { MenuAdminNavComponent } from './menu-admin-nav/menu-admin-nav.component'
import { ModelServiceService } from './model-service.service';
import { MenuAdminPosteditComponent } from './menu-admin-postedit/menu-admin-postedit.component';
import { FormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import {MatChipsModule} from '@angular/material/chips';
import {ChipsInputExample } from './chips-input-example/chips-input-example';
import {MatIconModule} from '@angular/material/icon';
import { BigPostComponent } from './big-post/big-post.component';
import { BigGaleriaComponent } from './big-galeria/big-galeria.component';
import { BigGaleriaTituloComponent } from './big-galeria-titulo/big-galeria-titulo.component';
import { SetTingsComponent } from './set-tings/set-tings.component';


@NgModule({
  declarations: [
    AppComponent,
    CardsInicioDisplayComponent,
    NavBarComponent,
    HomeComponent,
    GalleryComponent,
    LoginComponent,
    FooterComponent,
    RegistroComponent,
    LoginFormComponent,
    RegistroPageComponent,
    MenuAdminComponent,
    MenuAdminNavComponent,
    MenuAdminPosteditComponent,
    ProfileComponent,
    ChipsInputExample,
    BigPostComponent,
    BigGaleriaComponent,
    BigGaleriaTituloComponent,
    SetTingsComponent


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    MatChipsModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
