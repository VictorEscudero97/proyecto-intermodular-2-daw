import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ModelServiceService } from '../model-service.service';
import { ApiHttpService } from '../services/api-http.service';
import { ApiEndpointsService } from '../services/api-endpoints.service';
import { Constants } from '../config/constants';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
  providers: [ApiHttpService, ApiEndpointsService, Constants],
})
export class RegistroComponent implements OnInit {
  passwordCheck: boolean = false;
  @Input() isadmin: string = '';
  signupForm: FormGroup;
  inputEmail: string = '';

  selectedItemData: any;
  _subscription: any;

  action: string = '';

  closeResult: string = '';

  @Input() image!: any;
  @Input() edit: string = '';
  ischecked: number = 0;

  response_value!: any;
  mymodal: any;

  id: string = '';
  constructor(
    private _builder: FormBuilder,
    private _http: HttpClient,
    private modelServiceService: ModelServiceService,
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService,
    private modalService: NgbModal,
    private _router2: Router
  ) {
    this.signupForm = this._builder.group({
      username: ['', Validators.required],
      name: [''],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      texto: [''],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(8)]),
      ],
      password2: ['', Validators.required],
      admin: ['']
    });

    this.modelServiceService.cast.subscribe((result) => {
      this.selectedItemData = result;
      this.setFormData();
    });

    this.modelServiceService.type_cast.subscribe((result) => {
      this.action = result;
    });
  }

  ngOnInit(): void {
    if (this.edit != '') {
      this.action = 'editar';
      let cookie = localStorage.getItem('id');
      if (cookie != null) {
        this.id = cookie;
      }

      this.apiHttpService
        .get(this.apiEndpointsService.getUserbyId(this.id))
        .subscribe((result: any) => {
          console.log(result);
          this.selectedItemData = [];

          for (let i = 0; i < result.length; i++) {
            this.selectedItemData.push(result[i]);
          }

          this.signupForm.controls['name'].setValue(
            this.selectedItemData[0].name
          );
          this.signupForm.controls['username'].setValue(
            this.selectedItemData[0].usename
          );
          /*this.signupForm.controls['password'].setValue(
            this.selectedItemData[0].password
          );
          this.signupForm.controls['password2'].setValue(
            this.selectedItemData[0].password
          );*/
          this.signupForm.controls['email'].setValue(
            this.selectedItemData[0].email
          );
          this.signupForm.controls['texto'].setValue(
            this.selectedItemData[0].bio
          );
        });
    }
  }
  toggleEditable(event: any) {
    if (event.target!.checked) {
      this.ischecked = 1;
    } else {
      this.ischecked = 0;
    }
  }

  submitForm() {
    if (
      this.signupForm.get('password')!.value ==
      this.signupForm.get('password2')!.value
    ) {
      this.passwordCheck = false;
      var formData: any = new FormData();
      formData.append('name', this.signupForm.get('name')!.value);
      formData.append('username', this.signupForm.get('username')!.value);
      formData.append('email', this.signupForm.get('email')!.value);
      formData.append('password', this.signupForm.get('password')!.value);
      formData.append('bio', this.signupForm.get('texto')!.value);
      formData.append('photo', this.image);
      formData.append('admin', this.ischecked);

      var object: any = {};
      formData.forEach((value: any, key: any) => (object[key] = value));
      var json = JSON.stringify(object);
      //switch para elegir si crear, actualiar o eliminar
      switch (this.action) {
        case 'crear':
          this._http
            .post(this.apiEndpointsService.insertUser(), json)
            .subscribe(
              (response) => {
                this.response_value = response;
                console.log(response);
                if (this.response_value == 'Nuevo usuario guardado.') {
                  alert('Acción realizada con éxito');
                }
                else if(this.response_value == 'Usuario Repetido:'){
                  alert('Nombre de usuario o correo repetidos, prueba con otros');
                }
              },
              (error) => console.log(error)
            );
          break;
        case 'editar':
          this._http
            .post(
              this.apiEndpointsService.updateUserByUsername(
                this.selectedItemData.usename
              ),
              json
            )
            .subscribe(
              (response) =>{
                this.response_value = response;
                console.log(response);
                if (this.response_value == 'Usuario editado correctamente.') {
                  alert('Acción realizada con éxito');
                }
                else{
                  alert('Nombre de usuario o correo repetidos, prueba con otros');
                }
              },
              (error) => console.log(error)
            );
          break;
        case 'borrar':
          this._http
            .delete(
              this.apiEndpointsService.DeleteUserById(
                this.selectedItemData.id_user
              )
            )
            .subscribe(
              (response) => {
                this.response_value = response;
                console.log(response);
                if (this.response_value == 'Usuario eliminado correctamente.') {
                  alert('Acción realizada con éxito');
                }
                else{
                  alert('Ha ocurrido algún error contacta con la persona propietaria de la página');
                }
              },
              (error) => console.log(error)
            );
          break;
        default:
          console.log('action: ', this.action);
          this._http
            .post(this.apiEndpointsService.insertUser(), json)
            .subscribe(
              (response) => {
                this.response_value = response;
                console.log(response);
                if (this.response_value == 'Nuevo usuario guardado.') {
                  alert('Acción realizada con éxito');
                }
                else{
                  alert('Nombre de usuario o correo repetidos, prueba con otros');
                }
              },
              (error) => console.log(error)
            );
          break;
      }
      this.signupForm.reset();
    } else {
      this.passwordCheck = true;
    }
  }

  //funciones para en caso de tener un objeto seleccionado que cargue sus datos en el formulario
  setName() {
    if (this.selectedItemData != undefined) {
      this.signupForm.controls['name'].setValue(this.selectedItemData.name);
    }
  }

  setUsername() {
    if (this.selectedItemData != undefined) {
      this.signupForm.controls['username'].setValue(
        this.selectedItemData.usename
      );
    }
  }

  /*setPassword() {
    if (this.selectedItemData != undefined) {
      this.signupForm.controls['password'].setValue(
        this.selectedItemData.password
      );
    }
  }
  setPassword2() {
    if (this.selectedItemData != undefined) {
      this.signupForm.controls['password2'].setValue(
        this.selectedItemData.password
      );
    }
  }*/

  setEmail() {
    if (this.selectedItemData != undefined) {
      this.signupForm.controls['email'].setValue(this.selectedItemData.email);
    }
  }

  setTexto() {
    if (this.selectedItemData != undefined) {
      this.signupForm.controls['texto'].setValue(this.selectedItemData.bio);
    }
  }

  setFormData() {
    this.setName();
    this.setUsername();
    /*this.setPassword();
    this.setPassword2();*/
    this.setEmail();
    this.setTexto();
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) {
      return;
    }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', (e) => {
      this.image = reader.result as string;
    });
  }

}
