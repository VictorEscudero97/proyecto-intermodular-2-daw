import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuAdminPosteditComponent } from './menu-admin-postedit.component';

describe('MenuAdminPosteditComponent', () => {
  let component: MenuAdminPosteditComponent;
  let fixture: ComponentFixture<MenuAdminPosteditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuAdminPosteditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuAdminPosteditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
