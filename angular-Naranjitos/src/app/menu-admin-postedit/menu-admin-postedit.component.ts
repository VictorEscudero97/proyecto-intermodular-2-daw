import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { ApiHttpService } from '../services/api-http.service';
import { ApiEndpointsService } from '../services/api-endpoints.service';
import { Constants } from '../config/constants';
import { HttpClient } from '@angular/common/http';
import { TagServiceService } from '../services/tag-service.service';
import { ModelServiceService } from '../model-service.service';

@Component({
  selector: 'app-menu-admin-postedit',
  templateUrl: './menu-admin-postedit.component.html',
  styleUrls: ['./menu-admin-postedit.component.css'],
  providers: [ApiHttpService, ApiEndpointsService, Constants],
})
export class MenuAdminPosteditComponent implements OnInit {
  signupForm!: FormGroup;
  @Input() image: any = '';
  tags!: Array<any>;
  selectedItemData: any;
  action: string = '';
  approve_delete: string = '';
  response_value!: any;

  constructor(
    private _builder: FormBuilder,
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService,
    private _http: HttpClient,
    private _tagservice: TagServiceService,
    private modelServiceService: ModelServiceService
  ) {
    this.signupForm = this._builder.group({
      titulo: ['', Validators.required],
      texto: [''],
      tags: [''],
      tiempo: ['']
    });

    this._tagservice.cast.subscribe((result) => {
      this.tags = result;
    });

    this.modelServiceService.type_cast.subscribe((result) => {
      this.action = result;
    });

    this.modelServiceService.cast.subscribe((result) => {
      if (result != '') {
        this.selectedItemData = result;
        console.log(this.selectedItemData);
        this.setFormData();
      }
    });
  }

  ngOnInit(): void {}

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) {
      return;
    }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', (e) => {
      this.image = reader.result as string;
    });
  }

  submitForm() {
    if((this.image == null || this.image == undefined) && (this.selectedItemData.photo != null && this.selectedItemData.photo != undefined)){
      this.image = this.selectedItemData.photo;
    }
    if((this.tags == null || this.tags == undefined || this.tags.length == 0) && (this.selectedItemData.tag != null && this.selectedItemData.tag != undefined)){
      this.tags = this.selectedItemData.tag;
    }
    var formData: any = new FormData();
    formData.append('title', this.signupForm.get('titulo')!.value);
    formData.append('text', this.signupForm.get('texto')!.value);
    formData.append('tags', this.tags);
    formData.append('time', this.signupForm.get('tiempo')!.value);
    formData.append('photo', this.image);
    formData.append('id_user', localStorage.getItem('user'));

    var object: any = {};
    formData.forEach((value: any, key: any) => (object[key] = value));
    var json = JSON.stringify(object);
    //pillar variable del servicio
    /*this._http
        .post(
          this.apiEndpointsService.insertPost(),
          json
        )
        .subscribe(
          (response) => console.log(response),
          (error) => console.log(error)
        );*/
    switch (this.action) {
      case 'crear':
        this._http.post(this.apiEndpointsService.insertPost(), json).subscribe(
          (response) =>{
            console.log(response);
            this.response_value = response;
            if (this.response_value == 'Nuevo post guardado.') {
              alert('Acción realizada con éxito');
            }
            else if(this.response_value == 'Post Repetido'){
              alert('El titulo del post ya existe prueba con otro');
            }
          } ,
          (error) => console.log(error)
        );
        break;
      case 'editar':
        this._http.post(this.apiEndpointsService.updatePostById(this.selectedItemData.id_post), json).subscribe(
          (response) =>{
            console.log(response);
            this.response_value = response;
            if (this.response_value == 'Post editado correctamente.') {
              alert('Acción realizada con éxito');
            }
            else{
              alert('El titulo del post ya existe prueba con otro');
            }
          },
          (error) => console.log(error)
        );
        break;
      case 'borrar':
        this._http.delete(this.apiEndpointsService.DeletePostById(this.selectedItemData.id_post)).subscribe(
          (response) =>{
            console.log(response);
            this.response_value = response;
            if (this.response_value == 'Post eliminado correctamente.') {
              alert('Acción realizada con éxito');
            }
            else{
              alert('Ha ocurrido algún error contacta con la persona propietaria de la página');
            }
          },
          (error) => console.log(error)
        );
        break;
      case 'revisar':
        //si aprieto el verde que le haga un update de approved 0 to 1
        //si aprieto el rojo que lo elimine
        if(this.approve_delete == 'approve'){
          this._http.post(this.apiEndpointsService.approvePost(this.selectedItemData.id_post), json).subscribe(
            (response) =>{
              this.response_value = response;
              console.log(response);
              if (this.response_value == 'Post aprobado.') {
                alert('Acción realizada con éxito');
              }
              else{
                alert('Ha ocurrido algún error contacta con la persona propietaria de la página');
              }
            },
            (error) => console.log(error)
          );
        }
        else if(this.approve_delete == 'delete'){
          this._http.delete(this.apiEndpointsService.DeletePostById(this.selectedItemData.id_post)).subscribe(
            (response) =>{
              this.response_value = response;
              console.log(response);
              if (this.response_value == 'Post eliminado correctamente.') {
                alert('Acción realizada con éxito');
              }
              else{
                alert('Ha ocurrido algún error contacta con la persona propietaria de la página');
              }
            },
            (error) => console.log(error)
          );
        }
        break;
      default:
        this._http.post(this.apiEndpointsService.insertPost(), json).subscribe(
          (response) => console.log(response),
          (error) => console.log(error)
        );
        break;
    }
    this.signupForm.reset();
  }

  //funciones para en caso de tener un objeto seleccionado que cargue sus datos en el formulario
  setTitle() {
    if (this.selectedItemData != undefined) {
      this.signupForm.controls['titulo'].setValue(this.selectedItemData.title);
    }
  }

  setText() {
    if (this.selectedItemData != undefined) {
      this.signupForm.controls['texto'].setValue(this.selectedItemData.text);
    }
  }
  setTime() {
    if (this.selectedItemData != undefined) {
      this.signupForm.controls['tiempo'].setValue(this.selectedItemData.time);
    }
  }

  setFormData() {
    //console.log(this.selectedItemData);
    this.setTitle();
    this.setText();
    this.setTime();
  }

  setApprove_Delete(value: string){
    this.approve_delete = value;
  }
}
