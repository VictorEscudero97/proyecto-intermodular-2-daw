import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetTingsComponent } from './set-tings.component';

describe('SetTingsComponent', () => {
  let component: SetTingsComponent;
  let fixture: ComponentFixture<SetTingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetTingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetTingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
