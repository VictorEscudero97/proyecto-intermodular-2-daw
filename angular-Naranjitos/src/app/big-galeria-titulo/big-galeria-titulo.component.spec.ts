import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BigGaleriaTituloComponent } from './big-galeria-titulo.component';

describe('BigGaleriaTituloComponent', () => {
  let component: BigGaleriaTituloComponent;
  let fixture: ComponentFixture<BigGaleriaTituloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BigGaleriaTituloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BigGaleriaTituloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
