// Angular Modules
import { Injectable } from '@angular/core';
@Injectable()
export class Constants {
  public readonly API_ENDPOINT: string = 'http://localhost/Pruebaslim4/api_proyectointermodular/api';
  public readonly API_MOCK_ENDPOINT: string = 'mock-domain/api';
}
