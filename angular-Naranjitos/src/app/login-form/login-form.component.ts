import { Component, OnInit } from '@angular/core';
import { ApiHttpService } from '../services/api-http.service';
import { ApiEndpointsService } from '../services/api-endpoints.service';
import { Constants } from '../config/constants';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
  providers: [ApiHttpService, ApiEndpointsService, Constants]
})
export class LoginFormComponent implements OnInit {

  loginForm!: FormGroup;

  constructor(
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService,
    private _builder: FormBuilder,
    private _http: HttpClient,
    private _router: Router
  ) {

  }

  enviar(values:string) {
    console.log(values);
  }


  ngOnInit(): void {
    this.loginForm = this._builder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  submitForm() {
      /*var formData: any = new FormData();
      formData.append('username', this.loginForm.get('username')!.value);
      formData.append('password', this.loginForm.get('password')!.value);*/
      let username = this.loginForm.get('username')!.value;
      let password = this.loginForm.get('password')!.value;

      /*var object: any = {};
      formData.forEach((value: any, key: any) => (object[key] = value));
      var json = JSON.stringify(object);*/
      this._http
        .get(
          this.apiEndpointsService.getUserByUsernameAndPassword(username, password)
        )
        .subscribe(
          (response: any) =>
          {
            if(response != 'El usuario no ha sido encontrado'){
            localStorage.setItem('user', response[0].id_user);
            localStorage.setItem('photo', response[0].photo);
            localStorage.setItem('admin', response[0].admin);
            localStorage.setItem('name', response[0].usename);
            }


            location.reload();
            this._router.navigate(['/home']);

          },
          (error) => console.log(error)
        );
        this.loginForm.reset();
  }


}
