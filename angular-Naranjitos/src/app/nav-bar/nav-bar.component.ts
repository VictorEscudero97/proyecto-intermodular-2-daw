import { Component, OnInit } from '@angular/core';
import {
  faSearch
} from '@fortawesome/free-solid-svg-icons';
import { GaleryServiceService } from '../services/galery-service.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  faSearch = faSearch;
  u:any ='';
  img:any = '1';
  user: any ;
  closeResult: string = '';
  admin:any;
  p:any=0;
  constructor(
    private GaleryServiceService: GaleryServiceService,
    private _router2: Router,
    private modalService: NgbModal
  ) { }

  open(content: any) {
    let f = localStorage.getItem('user');
    console.log(f);
    if (f == undefined){
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
      this._router2.navigate(['/login']);
    }
  }

  private getDismissReason(reason: any): string {

    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }

  }

  ngOnInit(): void {
    this.user = null;
    this.user = localStorage.getItem('user')
    console.log('la v',this.user);
   this.img = localStorage.getItem('photo');

   this.admin = localStorage.getItem('admin');

  }

  buscar(value:string) {
    this.GaleryServiceService.setItem2(value);
    //this._router2.navigate(['/biggalery2',value]);
    this._router2.navigateByUrl('/', {skipLocationChange: true}).then(()=>{
      this._router2.navigate(['/biggalery2', value]);
    })
  }

  logout(){
    if (this.user != null){
      localStorage.clear();
      location.reload();
      this._router2.navigate(['/home']);
    }

  }
}
