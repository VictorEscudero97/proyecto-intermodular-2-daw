import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { ApiHttpService } from '../services/api-http.service';
import { ApiEndpointsService } from '../services/api-endpoints.service';
import { Constants } from '../config/constants';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ApiHttpService, ApiEndpointsService, Constants]
})
export class ProfileComponent implements OnInit {

  array_lista: Array<any> = [];
  id_user: string= '';
  constructor(
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService,
    private _router: ActivatedRoute
  ) { 
    this.id_user = this._router.snapshot.params.id;
    console.log(this._router.snapshot.params.id)

    this.apiHttpService
        .get(this.apiEndpointsService.getUserbyId(this.id_user))
        .subscribe((result: any) => {
          console.log(result);
          this.array_lista = [];
         
          for (let i = 0; i < result.length; i++) {
            this.array_lista.push(result[i]);
          }
          
        })
  }

  ngOnInit(): void {
    localStorage.setItem('id', this.id_user);
    console.log(console.log(localStorage.getItem('id')));
  }
}