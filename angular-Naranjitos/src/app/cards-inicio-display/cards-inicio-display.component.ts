import { Component, OnInit, Input } from '@angular/core';
//import { PeticionesService } from '../services/peticiones.service';
import {
  faCalendarAlt,
  faClock,
  faStar
} from '@fortawesome/free-solid-svg-icons';
//pruebas
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
//fin_pruebas
//prueba nueva forma request
import { ApiHttpService } from '../services/api-http.service';
import { ApiEndpointsService } from '../services/api-endpoints.service';
import { Constants } from '../config/constants';
//fin nueva forma request

import { PostServiceService } from '../services/post-service.service';
import { Router } from '@angular/router';


import { GaleryServiceService } from '../services/galery-service.service';
@Component({
  selector: 'app-cards-inicio-display',
  templateUrl: './cards-inicio-display.component.html',
  styleUrls: ['./cards-inicio-display.component.css'],
  providers: [ApiHttpService, ApiEndpointsService, Constants]
})
@Injectable()
export class CardsInicioDisplayComponent implements OnInit {
  faClock = faClock;
  faStar = faStar;
  facalendarAlt = faCalendarAlt;
  array_users: Array<any> = [];
  @Input() petition: string = '';
  @Input() template: string = '';
  @Input() id:string = '';
  r:any ='';
  selectedItem: any;
  selectedItemData: any;
  constructor(
    // Application Services
    private apiHttpService: ApiHttpService,
    private apiEndpointsService: ApiEndpointsService,
    private PostServiceService: PostServiceService,
    private GaleryServiceService: GaleryServiceService,
    private _router: Router
  ) {
    this.GaleryServiceService.cast.subscribe(
      (result)=>{
        this.selectedItemData = result;
        console.log(this.selectedItemData);

      })
  }

  ngOnInit(): void {

    switch (this.petition) {
      case 'all_posts':
        this.apiHttpService
          .get(this.apiEndpointsService.getAllPosts())
          .subscribe((result: any) => {
            //console.log(result);
            this.array_users = [];
            for (let i = 0; i < 3; i++) {
              this.array_users.push(result[i]);
            }
            //this.array_users.push(result.data);
            //console.log(this.array_users);
          });
        break;
      case 'user':
        this.apiHttpService
          .get(this.apiEndpointsService.getUserById('1'))
          .subscribe((result: any) => {
            //console.log(result );
            this.array_users.push(result);
            //console.log(this.array_users);
          });
        break;
      case 'user_posts':
        this.apiHttpService
        .get(this.apiEndpointsService.getUserPostsById('1'))
        .subscribe((result: any) => {
          console.log(result);
          this.array_users = [];
            for (let i = 0; i < result.length; i++) {
              this.array_users.push(result[i]);
            }
          console.log(this.array_users);
        });
        break;

      case 'title_posts':
        this.apiHttpService
        .get(this.apiEndpointsService.getPostsByTitle(this.selectedItemData))
        .subscribe((result: any) => {
          //console.log(result);
          this.array_users = [];
          if (result == 'El tag no ha sido encontrado'){
            this.r = result;
            console.log(this.array_users);
          }else{
            for (let i = 0; i < result.length; i++) {
              this.array_users.push(result[i]);
            }

              //console.log(this.array_users);

          }
        });
        break;


      case 'tag_posts':
        this.apiHttpService
          .get(this.apiEndpointsService.getPostsByTag(this.selectedItemData))
          .subscribe((result: any) => {
          console.log(result);
          this.array_users = [];
          if (result == 'El tag no ha sido encontrado'){
            this.r = result;
            console.log(this.array_users);
          }else{
            for (let i = 0; i < result.length; i++) {
              this.array_users.push(result[i]);
            }
          }

          //this.array_users.push(result.data);
          //console.log(this.array_users);
          });
      break;

      case 'id_posts':
        let id = localStorage.getItem('id');
        console.log(id);
        if (id !== null){
          this.apiHttpService
          .get(this.apiEndpointsService.getUserPostsById(id))
          .subscribe((result: any) => {
          console.log(result);
          this.array_users = [];
          if (result == 'No existen posts'){
            this.r = result;
            console.log(this.array_users);
          }else{
            for (let i = 0; i < result.length; i++) {
              this.array_users.push(result[i]);
            }

              //console.log(this.array_users);

          }
          });
        }
      break;

      case 'top_posts':
        this.apiHttpService
          .get(this.apiEndpointsService.getTopPost())
          .subscribe((result: any) => {
            //console.log(result);
            this.array_users = [];
            for (let i = 0; i < 3; i++) {
              this.array_users.push(result[i]);
            }
            //this.array_users.push(result.data);
            //console.log(this.array_users);
          });
        break;

      case 'date_posts':
        this.apiHttpService
          .get(this.apiEndpointsService.getDatePost())
          .subscribe((result: any) => {
            //console.log(result);
            this.array_users = [];
            for (let i = 0; i < 3; i++) {
              this.array_users.push(result[i]);
            }
              //this.array_users.push(result.data);
              //console.log(this.array_users);
          });
        break;
    }
  }

  getDataFromSelectedItem(data: any){
    for(let i = 0; i < this.array_users.length; i++){
      if(this.array_users[i].title == data){
        this.selectedItem = this.array_users[i];
        this.PostServiceService.setItem(<JSON>this.selectedItem);
        this._router.navigate(['/bigpost', this.selectedItem.title]);
      }
    }
  }
}
