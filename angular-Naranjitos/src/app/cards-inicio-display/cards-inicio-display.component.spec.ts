import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsInicioDisplayComponent } from './cards-inicio-display.component';

describe('CardsInicioDisplayComponent', () => {
  let component: CardsInicioDisplayComponent;
  let fixture: ComponentFixture<CardsInicioDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardsInicioDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsInicioDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
