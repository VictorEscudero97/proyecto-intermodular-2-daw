import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable({
  providedIn: 'root'
})
export class GaleryServiceService {
  private data = new BehaviorSubject<any>('');
  cast = this.data.asObservable();

  private data2 = new BehaviorSubject<any>('');
  cast2 = this.data2.asObservable();

  setItem(item: string){
    this.data.next(item);
  }

  setItem2(item: string){
    this.data2.next(item);
  }

  getItem(){
    return this.cast;

  }

  getItem2(){
    return this.cast2;

  }
  constructor() { }
}
