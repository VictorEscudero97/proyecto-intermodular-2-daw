// Angular Modules
import { Injectable } from '@angular/core';
// Application Classes
import { UrlBuilder } from '../url-builder';
import { QueryStringParameters } from '../query-string-parameters';
// Application Constants
import { Constants } from 'src/app/config/constants';
@Injectable()
export class ApiEndpointsService {
  constructor(
    // Application Constants
    private constants: Constants
  ) { }
  /* #region URL CREATOR */
  // URL
  private createUrl(
    action: string,
    isMockAPI: boolean = false
  ): string {
    const urlBuilder: UrlBuilder = new UrlBuilder(
      isMockAPI ? this.constants.API_MOCK_ENDPOINT :
      this.constants.API_ENDPOINT,
      action
    );
    return urlBuilder.toString();
  }
  // URL WITH QUERY PARAMS
  private createUrlWithQueryParameters(
    action: string,
    queryStringHandler?:
      (queryStringParameters: QueryStringParameters) => void
  ): string {
    const urlBuilder: UrlBuilder = new UrlBuilder(
      this.constants.API_ENDPOINT,
      action
    );
    // Push extra query string params
    if (queryStringHandler) {
      queryStringHandler(urlBuilder.queryString);
    }
    return urlBuilder.toString();
  }

  // URL WITH PATH VARIABLES
  private createUrlWithPathVariables(
    action: string,
    pathVariables: any[] = []
  ): string {
    let encodedPathVariablesUrl: string = '';
    // Push extra path variables
    for (const pathVariable of pathVariables) {
      if (pathVariable !== null) {
        encodedPathVariablesUrl +=
          `/${encodeURIComponent(pathVariable.toString())}`;
      }
    }
    const urlBuilder: UrlBuilder = new UrlBuilder(
      this.constants.API_ENDPOINT,
      `${action}${encodedPathVariablesUrl}`
    );
    return urlBuilder.toString();
  }
  /* #endregion */

  // USERS

  //GET Recueperar Users por ID (ok)
  public getUserById(
    id: string
  ): string{
    return this.createUrlWithPathVariables('users',[id]);
  }

  //GET Recueperar Users por username y password (ok)
  public getUserByUsernameAndPassword(
    username: string,
    password: string
  ): string{
    return this.createUrlWithPathVariables('users',[username, 'c',password]);
  }
  //GET Recueperar Posts por Users ID (ok)
  public getUserPostsById(
    id: string
  ): string{
    return this.createUrlWithPathVariables('users',[id, 'posts']);
  }

  //GET Recuperar todos los Users  (ok)
  public getAllUsers(): string{
    return this.createUrlWithPathVariables('users',['all']);
  }



  // POSTS

  //cambiar este a la ruta correcta cuando esté implementada
  public getAllPosts(): string{
    return this.createUrlWithPathVariables('posts',['top_rate']);
  }

  //POST Registrar usuario
  public insertUser(): string{
    return this.createUrlWithPathVariables('users',['new_user']);
  }

  //POST Introducir nuevo post
  public insertPost(): string{
    return this.createUrlWithPathVariables('posts',['new_post']);
  }

  //POST Actualizar post
  public updatePostById(
    id: string
  ): string{
    return this.createUrlWithPathVariables('admin',['edit_post', id]);
  }

  //POST Actualizar post
  public updateUserByUsername(
    username: string
  ): string{
    return this.createUrlWithPathVariables('admin',['edit_user', username]);
  }

  //DELETE Borrar usuario
  public DeleteUserById(
    id: string
  ){
    return this.createUrlWithPathVariables('admin', ['delete_user', id]);
  }

  //DELETE Borrar usuario
  public DeletePostById(
    id: string
  ){
    return this.createUrlWithPathVariables('admin', ['delete_post', id]);
  }

  //GET Recueperar Posts por tag (ok)
  public getPostsByTag(
    tag: string
  ): string{
    return this.createUrlWithPathVariables('post',[tag]);
  }


  //GET Recueperar Posts por tag (ok)
  public getPostsByTitle(
    title: string
  ): string{
    return this.createUrlWithPathVariables('title',[title]);
  }

  //GET Recueperar Posts por tag (ok)
  public getPostsByExactTitle(
    title: string
  ): string{
    return this.createUrlWithPathVariables('title',['exact',title]);
  }

  //GET Posts aprobados
  public getPostsApproved(): string{
    return this.createUrlWithPathVariables('admin',['approved','yes']);
  }

  //GET Posts no aprobados
  public getPostsUnapproved(): string{
    return this.createUrlWithPathVariables('admin',['approved','no']);
  }

   //POST Actualizar post set to approved
   public approvePost(
    id: string
  ): string{
    return this.createUrlWithPathVariables('admin',['edit_post', id, 'approve']);
  }

  //Get
  public getUserbyId(
    id: string
  ): string{
    return this.createUrlWithPathVariables('users',[id]);
  }

  //POST voted post in user table
  public vote(
    id: string
  ): string{
    return this.createUrlWithPathVariables('admin',['rate_user', id]);
  }

  //POST voted post in user table
  public post_rating_update(
    id: string
  ): string{
    return this.createUrlWithPathVariables('admin',['rate_post', id]);
  }

  //Get top posts
  public getTopPost(): string{
    return this.createUrlWithPathVariables('top');
  }

  //Get date posts
  public getDatePost(): string{
    return this.createUrlWithPathVariables('date');
  }


}
