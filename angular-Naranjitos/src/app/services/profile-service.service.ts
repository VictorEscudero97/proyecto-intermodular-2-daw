import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class PostServiceService {
  private data = new BehaviorSubject<any>('');
  cast = this.data.asObservable();

  setItem(item: JSON){
    this.data.next(item);
  }

  getItem(){
    return this.cast;

  }

  constructor() { }
}
