import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class TagServiceService {
  private data = new BehaviorSubject<any>([]);
  cast = this.data.asObservable();

  setItem(item: any){
    this.data.next(item);
  }

  getItem(){
    return this.cast;

  }

  constructor() { }
}
