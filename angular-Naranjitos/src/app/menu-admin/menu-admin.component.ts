import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-admin',
  templateUrl: './menu-admin.component.html',
  styleUrls: ['./menu-admin.component.css']
})
export class MenuAdminComponent implements OnInit {
  type: string = '';
  constructor() { }

  ngOnInit(): void {
  }

  setType(evento: any){
    this.type = evento;
    console.log('tipo: ',this.type);
  }

}
